package main

import "time"

func main() {
	v := NewUserValidator(NewLoggerUserFinder(SlowDataStore{}))
	v.IsAdmin(100)
	v.IsGuest(200)
}

//-

// User ...
type User struct {
	Role string
}

//go:generate ifacecodegen -source main.go -template logger.tmpl -destination logger.gen.go -package main

// UserFinder finds an existing User from a remote data store.
type UserFinder interface {
	Find(id int64) (User, error)
}

// UserValidator determines if the requested user has specific roles.
type UserValidator struct {
	f UserFinder
}

// NewUserValidator ...
func NewUserValidator(f UserFinder) UserValidator {
	return UserValidator{f}
}

// IsAdmin determines if the user with the specified id is an admin.
func (v *UserValidator) IsAdmin(id int64) (bool, error) {
	u, _ := v.f.Find(id) // XXX error validation omitted
	if u.Role == "admin" {
		return true, nil
	}
	return false, nil
}

// IsGuest determines if the user with the specified id is a guest.
func (v *UserValidator) IsGuest(id int64) (bool, error) {
	u, _ := v.f.Find(id) // XXX error validation omitted
	if u.Role == "guest" {
		return true, nil
	}
	return false, nil
}

//-

type SlowDataStore struct{}

func (d SlowDataStore) Find(id int64) (User, error) {
	time.Sleep(time.Duration(id) * time.Millisecond)
	return User{}, nil
}
