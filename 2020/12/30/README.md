# Using the C4 Model to document Software Architectures

[Original Link](https://mariocarrion.com/2020/12/30/documenting-software-architecture-c4-model.html)

This is a concrete example of the C4 model, using the system previously introduced during [Go Tools: counterfeiter](https://www.youtube.com/watch?v=ENqwq64TsDk), _Book Store System_:

## Running

Use the [Dockerfile](Dockerfile) to build the image locally with docker:

```
docker build -t c4:latest -f Dockerfile .
```

Then you can run it like:

```
docker run --rm -v $PWD:/img c4:latest /img/*.puml -o /img
```

## Level 1: Context

![Level 1 Context](level_1_context.png)

## Level 2: Container

![Level 2 Container](level_2_container.png)

## Level 3: Component - Admin Web API

![Level 3 Component - Admin Web API](level_3_component_admin_web_api.png)

> Similar diagrams shouls be implemented for all the other components.
