// +build !wasm

package main

import (
	"flag"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"path/filepath"

	"github.com/lpar/gzipped/v2"
	"github.com/vugu/vugu/simplehttp"
)

var (
	defaulTemplateSource = `<!doctype html>
<html>
<head>
<title>Vugu Example</title>

<meta charset="utf-8"/>

<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-giJF6kkoqNQ00vy+HMDP7azOuL0xtbfIcaT9wjKHr8RbDVddVHyTfAAsrekwKmP1" crossorigin="anonymous">

<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js" integrity="sha384-ygbV9kiqUc6oa4msXn9868pTtWMgiQaeYH7/t7LECLbyPA2x65Kgf80OJFdroafW" crossorigin="anonymous"></script>

<script src="https://cdn.jsdelivr.net/npm/text-encoding@0.7.0/lib/encoding.min.js"></script> <!-- MS Edge polyfill -->
<script src="/wasm_exec.js"></script>

</head>
<body>
<div id="vugu_mount_point">
	<div class="d-flex justify-content-center" style="position: absolute; top: 50%%; left: 50%%;">
		Loading... &nbsp;
		<div class="spinner-border text-primary" role="status">
			<span class="visually-hidden">Loading...</span>
		</div>
	</div>
	</div>
<script>
function downloadFile(buf, fileType, fileName) {
	let blob = new Blob([buf], {'type': fileType});
	var a = document.createElement('a');
	a.href = window.URL.createObjectURL(blob);
	a.download = fileName;
	a.dispatchEvent(new MouseEvent('click'));
}
</script>
<script>
var wasmSupported = (typeof WebAssembly === "object");
if (wasmSupported) {
	if (!WebAssembly.instantiateStreaming) { // polyfill
		WebAssembly.instantiateStreaming = async (resp, importObject) => {
			const source = await (await resp).arrayBuffer();
			return await WebAssembly.instantiate(source, importObject);
		};
	}
	const go = new Go();
	WebAssembly.instantiateStreaming(fetch("/main.wasm"), go.importObject).then((result) => {
		go.argv = ["main.wasm", "-greeting", "%s"];
		go.run(result.instance);
	});
} else {
	document.getElementById("vugu_mount_point").innerHTML = 'This application requires WebAssembly support. Please upgrade your browser.';
}
</script>
</body>
</html>`
)

func main() {
	var (
		dir, greeting string
		httpPort      int
	)

	flag.IntVar(&httpPort, "port", 8888, "HTTP port to listen to.")
	flag.StringVar(&dir, "dir", " ./dist", "Project directory")
	flag.StringVar(&greeting, "greeting", " Hola", "Greeting to pass in to WASM")
	flag.Parse()

	//-

	templ := fmt.Sprintf(defaulTemplateSource, greeting)

	wd, _ := filepath.Abs(dir)

	h := simplehttp.New(wd, false)

	health := func(h http.Handler) http.Handler {
		return http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
			if r.URL.Path == "/health" {
				w.WriteHeader(http.StatusOK)
				return
			}

			h.ServeHTTP(w, r)
		})
	}

	h.StaticHandler = gzipped.FileServer(gzipped.Dir(wd))

	h.PageHandler = health(&simplehttp.PageHandler{
		Template:         template.Must(template.New("_page_").Parse(templ)),
		TemplateDataFunc: simplehttp.DefaultTemplateDataFunc,
	})

	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", httpPort), h))
}
