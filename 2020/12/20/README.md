# WebAssembly + Go + Vugu Tips

[Original Link](https://mariocarrion.com/2020/12/20/go-tips-vugu-webassembly-wasm-tips.html)

## Tips

1. Custom template for rendering Vugu Pages.
1. wasm `main` arguments via Javascript.
1. `/health` check using built-in Vugu's `simplehttp` server (via `github.com/vugu/vugu/simplehttp`)
1. Using `gzipped.FileServer` (via `github.com/lpar/gzipped/v2`) to render smaller wasm files.
1. Downloading a file.

## Requirements

* `go` version 1.15 or greater
* `brotli` for compressing the WASM file

## Building

```sh
rm -rf dist
mkdir dist
go generate gitlab.com/MarioCarrion/blog-examples/2020/12/20
cp "$(go env GOROOT)/misc/wasm/wasm_exec.js" dist/
GOOS=js GOARCH=wasm go build -o dist/main.wasm gitlab.com/MarioCarrion/blog-examples/2020/12/20
brotli -o dist/main.wasm.br dist/main.wasm
go build -o server gitlab.com/MarioCarrion/blog-examples/2020/12/20
./server -dir dist/
```

You can also use `-greeting` when running the server to change the message:

```sh
./server -dir dist -greeting "Kumusta po"
```
