# Go Swagger + Swagger UI + CORS

Tested using

* `go` **1.10**
* `dep` **v0.4.1**
* `docker` **18.06.0-ce-mac70 (26399)**

## dep

* `dep ensure -vendor-only`

## go-swagger

* Install [go-swagger](https://goswagger.io/install.html#installing-from-source)
* Run `go generate ./...`

## Swagger UI

* `docker run --rm -d -p 8080:8080 -e API_URL=http://localhost:8000/swagger.json swaggerapi/swagger-ui:3.17.5`
* Open `http://localhost:8080/`
* Click explore
