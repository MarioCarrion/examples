// Package main Provides all Users APIs.
//
// HTTP APIs used as an example
//
//  Schemes: http, https
//  Host: localhost:8000
//  BasePath: /
//  Version: 1.0.0
//  Contact: info@carrion.ws
//  Consumes:
//    - application/json
//  Produces:
//    - application/json
//
// swagger:meta
//
//go:generate swagger generate spec -m -o swagger.json
//
package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"os"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
)

// User represents the user in the system.
// swagger:model
type User struct {
	Name    string `json:"name"`
	Address string `json:"address"`
}

func main() {
	r := mux.NewRouter()
	r.HandleFunc("/users", getUsersHandler).Methods(http.MethodGet)
	r.HandleFunc("/swagger.json", func(w http.ResponseWriter, r *http.Request) {
		http.ServeFile(w, r, "swagger.json")
	}).Methods(http.MethodGet)

	http.Handle("/", r)

	headersOk := handlers.AllowedHeaders([]string{"X-Requested-With", "Authorization", "Content-Type"})
	originsOk := handlers.AllowedOrigins([]string{"*"})
	methodsOk := handlers.AllowedMethods([]string{http.MethodGet, http.MethodPost, http.MethodPut, http.MethodDelete, http.MethodOptions})

	http.ListenAndServe(":8000", handlers.CORS(originsOk, headersOk, methodsOk)(r))
}

// swagger:route GET /users users getUsers
//
// Returns a list of dummy users.
//
// ---
// Responses:
//   200: []User
func getUsersHandler(w http.ResponseWriter, r *http.Request) {
	users := []User{{"Mario Carrion", "Address 123"}, {"John Doe", "Address 890"}}
	marshal, _ := json.Marshal(users)

	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(http.StatusOK)

	if _, err := w.Write(marshal); err != nil {
		fmt.Fprintf(os.Stderr, "Received error %s\n", err)
	}
}
