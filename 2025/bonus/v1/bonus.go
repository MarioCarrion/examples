package bonus

var Default = Calculator{
	names: map[string]int{
		"Mario": 10,
		"Ruby":  15,
	},
}

var DefaultValue = 8

type Calculator struct {
	names map[string]int
}

func NewCalculator(names map[string]int) Calculator {
	return Calculator{names: names}
}

func (c Calculator) Percentage(name string) int {
	v, ok := c.names[name]
	if !ok {
		return DefaultValue
	}

	return v
}
