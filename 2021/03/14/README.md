# README

[Original blog post](https://mariocarrion.com/2021/03/14/golang-package-testing-datastores-ory-dockertest.html)

## Requirements

1. Docker, you can download in advance the memcached image if you want, but not necessary. 
    ```
    docker pull memcached:1.6.9-alpine
    ```
1. Go 1.16
1. [`direnv`](https://mariocarrion.com/2020/11/20/golang-go-tool-direnv.html): for _sandboxing_ the required tools used by this project.
1. [`counterfeiter`](https://mariocarrion.com/2019/06/24/golang-tools-counterfeiter.html): for generating mocks.
    ```
    go install github.com/maxbrunsfeld/counterfeiter/v6@v6.3.0
    ```

## Running

```
go test ./...
```
