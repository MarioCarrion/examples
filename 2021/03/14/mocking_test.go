package mocking_test

import (
	"bytes"
	"encoding/gob"
	"fmt"
	"net"
	"runtime"
	"testing"
	"time"

	"github.com/bradfitz/gomemcache/memcache"
	"github.com/ory/dockertest/v3"
	"github.com/ory/dockertest/v3/docker"

	mocking "gitlab.com/MarioCarrion/blog-examples/2021/03/14-mocking"
	"gitlab.com/MarioCarrion/blog-examples/2021/03/14-mocking/mockingtesting"
)

func TestAdapterMemcached(t *testing.T) {
	// TODO: Test sad/unhappy-paths cases

	mock := mockingtesting.FakeMemcacheClient{}
	mock.GetReturns(&memcache.Item{
		Value: func() []byte {
			var b bytes.Buffer
			_ = gob.NewEncoder(&b).Encode("value")
			return b.Bytes()
		}(),
	}, nil)

	c := mocking.NewAdapterMemcached(&mock)

	if err := c.Set("key", "value"); err != nil {
		t.Fatalf("expected no error, got %s", err)
	}

	value, err := c.Get("key")
	if err != nil {
		t.Fatalf("expected no error, got %s", err)
	}

	if value != "value" {
		t.Fatalf("expected `value`, got %s", value)
	}
}

func TestConcreteMemcached(t *testing.T) {
	// TODO: Test sad/unhappy-paths cases

	client := newClient(t)

	c := mocking.NewConcreteMemcached(client)

	if err := c.Set("concrete-key", "value"); err != nil {
		t.Fatalf("expected no error, got %s", err)
	}

	value, err := c.Get("concrete-key")
	if err != nil {
		t.Fatalf("expected no error, got %s", err)
	}

	if value != "value" {
		t.Fatalf("expected `value`, got %s", value)
	}
}

func newClient(tb testing.TB) *memcache.Client {
	pool, err := dockertest.NewPool("")
	if err != nil {
		tb.Fatalf("Could not instantiate docker pool: %s", err)
	}

	pool.MaxWait = 2 * time.Second

	resource, err := pool.RunWithOptions(&dockertest.RunOptions{
		Repository: "memcached",
		Tag:        "1.6.6-alpine",
	}, func(config *docker.HostConfig) {
		config.AutoRemove = true
		config.RestartPolicy = docker.RestartPolicy{
			Name: "no",
		}
	})

	if err != nil {
		tb.Fatalf("Could not run container: %s", err)
	}

	tb.Cleanup(func() {
		if err := pool.Purge(resource); err != nil {
			tb.Fatalf("Could not purge container: %v", err)
		}
	})

	addr := fmt.Sprintf("%s:11211", resource.Container.NetworkSettings.IPAddress)
	if runtime.GOOS == "darwin" { // XXX: network layer is different on Mac
		addr = net.JoinHostPort(resource.GetBoundIP("11211/tcp"), resource.GetPort("11211/tcp"))
	}

	if err := pool.Retry(func() error {
		var ss memcache.ServerList
		if err := ss.SetServers(addr); err != nil {
			return err
		}

		return memcache.NewFromSelector(&ss).Ping()
	}); err != nil {
		tb.Fatalf("Could not connect to memcached: %s", err)
	}

	return memcache.New(addr)
}
