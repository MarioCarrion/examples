module gitlab.com/MarioCarrion/blog-examples/2021/03/14-mocking

go 1.16

require (
	github.com/bradfitz/gomemcache v0.0.0-20190913173617-a41fca850d0b
	github.com/ory/dockertest/v3 v3.6.3
)
